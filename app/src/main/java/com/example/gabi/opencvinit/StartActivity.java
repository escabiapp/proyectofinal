package com.example.gabi.opencvinit;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class StartActivity extends Activity {

    Button btnMedir ;
    Button btnMarcar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        btnMedir = (Button) findViewById(R.id.btnMedir);
        btnMedir.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(StartActivity.this, MainActivity.class));
            }
        });

        btnMarcar = (Button) findViewById(R.id.btnMarcar);
        btnMarcar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StartActivity.this, NivelActivity.class));
            }
        });
    }


}
