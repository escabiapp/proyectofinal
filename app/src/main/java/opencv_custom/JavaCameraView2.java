package opencv_custom;

import android.content.Context;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.util.Log;

import org.opencv.android.JavaCameraView;

import java.util.List;

/**
 * Created by gabriel on 9/6/2016.
 */
public class JavaCameraView2 extends JavaCameraView {

    private static final String TAG = "JavaCameraView2";
    private static boolean isFlashLightON = false;
    private Context myreference;

    public JavaCameraView2(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.myreference = context;
    }

    public List<String> getEffectList() {
        return mCamera.getParameters().getSupportedColorEffects();
    }

    public boolean isEffectSupported() {
        return (mCamera.getParameters().getColorEffect() != null);
    }

    public String getEffect() {
        return mCamera.getParameters().getColorEffect();
    }

    public void setEffect(String effect) {
        Camera.Parameters params = mCamera.getParameters();
        params.setColorEffect(effect);
        mCamera.setParameters(params);
    }

    public List<Camera.Size> getResolutionList() {
        return mCamera.getParameters().getSupportedPreviewSizes();
    }

    public Camera.Size getResolution() {
        return mCamera.getParameters().getPreviewSize();
    }

    public void setResolution(Camera.Size resolution) {
        disconnectCamera();
        mMaxHeight = resolution.height;
        mMaxWidth = resolution.width;
        connectCamera(getWidth(), getHeight());
    }

    // Setup the camera
    public void setupCameraFlashLight() {
        Camera camera = mCamera;
        if (camera != null) {
            Camera.Parameters params = camera.getParameters();

            if (params != null) {
                if (isFlashLightON) {
                    isFlashLightON = false;
                    params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    camera.setParameters(params);
                    camera.startPreview();
                    Log.e(TAG, "setupCameraFlashLight: Deberia haber apagado el flash");
                } else {
                    isFlashLightON = true;
                    params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    camera.setParameters(params);
                    camera.startPreview();
                    Log.e(TAG, "setupCameraFlashLight: Deberia haber arrancado el flash");
                }
            }
        }

    }

    public void setupCameraFlashLightOn() {
        Camera camera = mCamera;
        if (camera != null) {
            Camera.Parameters params = camera.getParameters();


            isFlashLightON = true;
            params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            camera.setParameters(params);
            camera.startPreview();
            Log.e(TAG, "setupCameraFlashLightOn: Deberia haber arrancado el flash");


        }
    }

    public void setupCameraFlashLightOff() {
        Camera camera = mCamera;
        if (camera != null) {
            Camera.Parameters params = camera.getParameters();


            isFlashLightON = false;
            params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            camera.setParameters(params);
            camera.startPreview();
            Log.e(TAG, "setupCameraFlashLightOff: Deberia haber apagado el flash");

        }
    }


}
