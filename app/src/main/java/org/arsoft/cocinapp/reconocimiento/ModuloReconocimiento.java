package org.arsoft.cocinapp.reconocimiento;

/**
 * Created by Porron on 17/7/16.
 */
public class ModuloReconocimiento {

    private static ModuloReconocimiento ourInstance = new ModuloReconocimiento();

    public static ModuloReconocimiento getInstance() {
        return ourInstance;
    }

    private ModuloReconocimiento() {
    }
}
